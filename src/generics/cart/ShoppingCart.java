package generics.cart;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class ShoppingCart<T extends CartItem> {

    private List<T> cart = new ArrayList<>();
    private Double finalDisc = 1.0;
    private Double prevDisc = 0.0;

    public void add(T item) {
        cart.add(item);
    }

    public void removeById(String id) {
        cart.removeIf(item -> item.getId().equals(id));
    }

    public Double getTotal() {
        Double sum = 0.0;
        for (T item : cart) {
            sum += item.getPrice();
        }
        return sum * finalDisc;
    }

    public void increaseQuantity(String id) {
        for (T item : cart) {
            if (item.getId().equals(id)){
                cart.add(item);
                break;
            }
        }
    }

    public void applyDiscountPercentage(Double discount) {
        prevDisc = (100 - discount) / 100;
        finalDisc *= prevDisc;
    }

    public void removeLastDiscount() {
        finalDisc /= prevDisc;
    }

    public void addAll(List<T> items) {
        for (T item : items) {
            add(item);
        }

    }

    @Override
    public String toString(){
        LinkedList<List<T>> temp = new LinkedList<>();

        for (T item : cart) {
            String currentItemID = item.getId();
            if (temp.isEmpty()) {
                temp.add(new LinkedList<>(Collections.singletonList(item)));
            } else if (temp.get(temp.size() - 1).get(0).getId().equals(currentItemID)) {
                temp.getLast().add(item);
            } else {
                temp.add(new LinkedList<>(Collections.singletonList(item)));
            }
        }

        List<String> result = new ArrayList<>();
        for (List<T> item : temp) {
            result.add("(" + item.get(0).getId() + ", " + item.get(0).getPrice() + ", " + item.size() +")");
        }
        return result.toString().replaceAll("\\[", "").replaceAll("]","");
    }
}
