package generics.stack;

public class Stack {

    private Object[] elements;
    private Integer size = 0;

    public Stack() {
        elements = new Object[100];
    }

    public void push(Object element) {
        elements[size++] = element;
    }

    public Object pop() {
        if (size == 0) {
            throw new IllegalStateException("stack is empty");
        }

        return elements[--size];
    }

}
