package generics.pair;

public class Pair<T, B> {

    private T first;
    private B second;

    public Pair(T first, B second) {
        this.first = first;
        this.second = second;
    }

    public T getFirst() {
        return first;
    }

    public B getSecond() {
        return second;
    }

}
