package collections.set;

import oo.hide.Point;
import org.junit.Test;

import java.util.HashSet;
import java.util.Set;


public class HashCodeExample {

    @Test
    public void runExample() {

        Set<Integer> set = new HashSet<>();

        set.add(new Point(1, 1).hashCode());
        set.add(new Point(1, 1).hashCode());

        System.out.println(set.size());

    }
}
