package collections.streaks;

import java.util.*;

public class Code {

    public static List<List<String>> getStreakList(String input) {
        List<List<String>> list = new ArrayList<>();
        if (input.equals("")){
            return list;
        }
        List<String> listToBeIn = new ArrayList<>();
        for (int i = 0; i < input.length(); i++){
            if (i > 0 && input.charAt(i-1) == input.charAt(i)){
                listToBeIn.add(String.valueOf(input.charAt(i)));
            } else{
                if (i > 0){
                    list.add(listToBeIn);
                }
                listToBeIn = new ArrayList<>();
                listToBeIn.add(String.valueOf(input.charAt(i)));
                if (input.length() == 1 || i == input.length() - 1){
                    list.add(listToBeIn);
                }
            }
        }
        return list;
    }


}
