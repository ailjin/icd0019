package collections.cache;

import java.util.HashMap;
import java.util.Map;

public class Fibonacci {

    Map<Integer, Integer> map = new HashMap<>();

    public Fibonacci() {
        caching();
    }

    public void caching(){
        for (int i = 0; i < 41; i++){
            map.put(i, fib(i));
        }
    }

    public Integer fib(Integer n) {
        if (map.containsKey(n)){
            return map.get(n);
        }
        if (n < 1) {
            return 0;
        }
        if (n == 1) {
            return 1;
        }
        return fib(n - 1) + fib(n - 2);
    }

}
