package collections.simulator;

import java.util.Objects;

public class Card implements Comparable<Card> {

    public enum CardValue { S2, S3, S4, S5, S6, S7, S8, S9, S10, J, Q, K, A }

    public enum CardSuit { C, D, H, S }

    private final CardValue value;
    private final CardSuit suit;
    private Integer number = 2;

    public Card(CardValue value, CardSuit suit) {
        this.value = value;
        this.suit = suit;
        if (value == CardValue.S3){
            this.number = 3;
        }else if (value == CardValue.S4){
            this.number = 4;
        }else if (value == CardValue.S5){
            this.number = 5;
        }else if (value == CardValue.S6){
            this.number = 6;
        }else if (value == CardValue.S7){
            this.number = 7;
        }else if (value == CardValue.S8){
            this.number = 8;
        }else if (value == CardValue.S9){
            this.number = 9;
        }else if (value == CardValue.S10){
            this.number = 10;
        }else if (value == CardValue.J){
            this.number = 11;
        }else if (value == CardValue.Q){
            this.number = 12;
        }else if (value == CardValue.K){
            this.number = 13;
        }else if (value == CardValue.A){
            this.number = 14;
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Card)) {
            return false;
        }

        Card other = (Card) obj;

        return Objects.equals(value, other.value)
                && Objects.equals(suit, other.suit);
    }

    @Override
    public int compareTo(Card other) {
        return this.number - other.number;
    }

    public CardValue getValue() {
        return value;
    }

    public CardSuit getSuit() {
        return suit;
    }

    @Override
    public String toString() {
        return String.format("(%s, %s)", value, suit);
    }
}
