package collections.simulator;

import java.util.*;

public class Hand implements Iterable<Card> {

    private List<Card> cards = new ArrayList<>();

    public void addCard(Card card) {
        cards.add(card);
    }

    @Override
    public String toString() {
        return cards.toString();
    }

    public boolean checkIfFourOfAKindOf4(List<Card> cards){
        return cards.get(0).getValue() == cards.get(1).getValue()
                && cards.get(1).getValue() == cards.get(2).getValue()
                && cards.get(2).getValue() == cards.get(3).getValue();
    }

    public boolean checkIfTripsOf4(List<Card> cards){
        return cards.get(0).getValue() == cards.get(1).getValue()
                && cards.get(1).getValue() == cards.get(2).getValue()
                || cards.get(1).getValue() == cards.get(2).getValue()
                && cards.get(2).getValue() == cards.get(3).getValue();
    }

    public boolean checkIfTwoPairsOf4(List<Card> cards){
        return cards.get(0).getValue() == cards.get(1).getValue()
                && cards.get(2).getValue() == cards.get(3).getValue();
    }

    public boolean checkIfOnePairOf4(List<Card> cards){
        return cards.get(0).getValue() == cards.get(1).getValue()
                || cards.get(1).getValue() == cards.get(2).getValue()
                || cards.get(2).getValue() == cards.get(3).getValue();
    }

    public HandType lowSizeHandType(){
        if (cards.size() == 2){
            if (cards.get(0).getValue() == cards.get(1).getValue()){
                return HandType.ONE_PAIR;
            }else{
                return HandType.HIGH_CARD;
            }
        }else if(cards.size() == 3){
            if (cards.get(0).getValue() == cards.get(1).getValue()
                    && cards.get(1).getValue() == cards.get(2).getValue()){
                return HandType.TRIPS;
            }else if (cards.get(0).getValue() == cards.get(1).getValue()
                    || cards.get(0).getValue() == cards.get(2).getValue()
                    || cards.get(1).getValue() == cards.get(2).getValue()){
                return HandType.ONE_PAIR;
            }else{
                return HandType.HIGH_CARD;
            }
        }else {
            if (checkIfFourOfAKindOf4(cards)){
                return HandType.FOUR_OF_A_KIND;
            }else if (checkIfTripsOf4(cards)){
                return HandType.TRIPS;
            }else if (checkIfTwoPairsOf4(cards)){
                return HandType.TWO_PAIRS;
            }else if (checkIfOnePairOf4(cards)){
                return HandType.ONE_PAIR;
            }else{
                return HandType.HIGH_CARD;
            }
        }
    }

    public boolean checkIfStraightFlush(List<Card> cards){
        return cards.get(0).compareTo(cards.get(1)) == -1 && cards.get(1).compareTo(cards.get(2)) == -1
                && cards.get(2).compareTo(cards.get(3)) == -1 && cards.get(3).compareTo(cards.get(4)) == -1
                && cards.get(0).getSuit() == cards.get(1).getSuit()
                && cards.get(1).getSuit() == cards.get(2).getSuit()
                && cards.get(2).getSuit() == cards.get(3).getSuit()
                && cards.get(3).getSuit() == cards.get(4).getSuit();
    }

    public boolean checkIfFourOfAKind(List<Card> cards){
        return cards.get(0).getValue() == cards.get(1).getValue()
                && cards.get(1).getValue() == cards.get(2).getValue()
                && cards.get(2).getValue() == cards.get(3).getValue()
                || cards.get(1).getValue() == cards.get(2).getValue()
                && cards.get(2).getValue() == cards.get(3).getValue()
                && cards.get(3).getValue() == cards.get(4).getValue();
    }

    public boolean checkIfFullHouse(List<Card> cards){
        return cards.get(0).getValue() == cards.get(1).getValue()
                && cards.get(2).getValue() == cards.get(3).getValue()
                && cards.get(3).getValue() == cards.get(4).getValue()
                || cards.get(0).getValue() == cards.get(1).getValue()
                && cards.get(1).getValue() == cards.get(2).getValue()
                && cards.get(3).getValue() == cards.get(4).getValue();
    }

    public boolean checkIfFlush(List<Card> cards){
        return cards.get(0).getSuit() == cards.get(1).getSuit()
                && cards.get(1).getSuit() == cards.get(2).getSuit()
                && cards.get(2).getSuit() == cards.get(3).getSuit()
                && cards.get(3).getSuit() == cards.get(4).getSuit();
    }

    public boolean checkIfStraight(List<Card> cards){
        return cards.get(0).compareTo(cards.get(1)) == -1 && cards.get(1).compareTo(cards.get(2)) == -1
                && cards.get(2).compareTo(cards.get(3)) == -1 && cards.get(3).compareTo(cards.get(4)) == -1
                || cards.get(0).getValue() == Card.CardValue.S2 && cards.get(1).getValue() == Card.CardValue.S3
                && cards.get(2).getValue() == Card.CardValue.S4 && cards.get(3).getValue() == Card.CardValue.S5
                && cards.get(4).getValue() == Card.CardValue.A;
    }

    public boolean checkIfTrips(List<Card> cards){
        return cards.get(0).getValue() == cards.get(1).getValue()
                && cards.get(1).getValue() == cards.get(2).getValue()
                || cards.get(1).getValue() == cards.get(2).getValue()
                && cards.get(2).getValue() == cards.get(3).getValue()
                || cards.get(2).getValue() == cards.get(3).getValue()
                && cards.get(3).getValue() == cards.get(4).getValue();
    }

    public boolean checkIfTwoPairs(List<Card> cards){
        return (cards.get(0).getValue() == cards.get(1).getValue())
                && (cards.get(2).getValue() == cards.get(3).getValue())
                || (cards.get(1).getValue() == cards.get(2).getValue())
                && (cards.get(3).getValue() == cards.get(4).getValue());
    }

    public boolean checkIfOnePair(List<Card> cards){
        return cards.get(0).getValue() == cards.get(1).getValue()
                || cards.get(1).getValue() == cards.get(2).getValue()
                || cards.get(2).getValue() == cards.get(3).getValue()
                || cards.get(3).getValue() == cards.get(4).getValue();
    }

    public HandType getHandType() {
        Collections.sort(cards);
        if (cards.size() < 5){
            return lowSizeHandType();
        }else {
            if (checkIfStraightFlush(cards)){
                return HandType.STRAIGHT_FLUSH;
            }else if (checkIfFourOfAKind(cards)){
                return HandType.FOUR_OF_A_KIND;
            }else if (checkIfFullHouse(cards)){
                return HandType.FULL_HOUSE;
            }else if (checkIfFlush(cards)){
                return HandType.FLUSH;
            }else if (checkIfStraight(cards)){
                return HandType.STRAIGHT;
            }else if (checkIfTrips(cards)){
                return HandType.TRIPS;
            }else if (checkIfTwoPairs(cards)){
                return HandType.TWO_PAIRS;
            }else if (checkIfOnePair(cards)){
                return HandType.ONE_PAIR;
            }else {
                return HandType.HIGH_CARD;
            }
        }
    }

    public boolean contains(Card card) {
        return cards.contains(card);
    }

    public boolean isEmpty() {
        return cards.isEmpty();
    }

    @Override
    public Iterator<Card> iterator() {
        return cards.iterator();
    }
}
