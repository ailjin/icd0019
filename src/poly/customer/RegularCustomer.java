package poly.customer;

import java.time.LocalDate;

public class RegularCustomer extends AbstractCustomer {

    public LocalDate lastOrderDate;

    public RegularCustomer(String id, String name,
                           int bonusPoints, LocalDate lastOrderDate) {

        super(id, name, bonusPoints);
        this.lastOrderDate = lastOrderDate;
    }

    @Override
    public void collectBonusPointsFrom(Order order) {
        if (order.getTotal() >= 100){
            this.bonusPoints += order.getTotal();
        }
        if (order.getTotal() >= 100 && order.getDate().isBefore(lastOrderDate.plusMonths(1))){
            this.bonusPoints += order.getTotal() / 2;
        }
        this.lastOrderDate = order.getDate();
    }


    @Override
    public String asString() {
        return "ID: " + this.id + ";Name: " + this.name + ";Bonus Points: " + this.bonusPoints;
    }

}