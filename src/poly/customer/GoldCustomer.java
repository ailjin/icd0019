package poly.customer;

public class GoldCustomer extends AbstractCustomer {

    public GoldCustomer(String id, String name,
                        int bonusPoints) {

        super(id, name, bonusPoints);
        this.id = id;
        this.name = name;
        this.bonusPoints = bonusPoints;
    }

    @Override
    public void collectBonusPointsFrom(Order order) {
        this.bonusPoints += order.getTotal() * 1.5;
    }

    @Override
    public String asString() {
        return "ID: " + this.id + ";Name: " + this.name + ";Bonus Points: " + this.bonusPoints;
    }

}