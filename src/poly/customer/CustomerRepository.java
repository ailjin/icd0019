package poly.customer;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class CustomerRepository {

    public ArrayList<AbstractCustomer> customers;
    private static final Path FILE_PATH = Path.of("src/poly/customer/data.txt");

    public DateTimeFormatter formatter = DateTimeFormatter
            .ofPattern("yyyy-MM-dd");

    public CustomerRepository() {
        try {
            ArrayList<AbstractCustomer> customers = new ArrayList<>();
            for (String line : Files.readAllLines(FILE_PATH)) {
                String[] data = line.split(";");
                if (data[0].equals("REGULAR")) {
                    RegularCustomer r = new RegularCustomer(data[1], data[2], Integer.parseInt(data[3]),
                            LocalDate.parse(data[4], formatter));
                    customers.add(r);
                } else {
                    GoldCustomer g = new GoldCustomer(data[1], data[2], Integer.parseInt(data[3]));
                    customers.add(g);
                }
            }
            this.customers = customers;
        } catch (IOException e) {
            throw new RuntimeException("there is no such file");
        }
    }

    public Optional<AbstractCustomer> getCustomerById(String id) {
        for (AbstractCustomer c : this.customers){
            if (c.id.equals(id)){
                return Optional.of(c);
            }
        }
        return Optional.empty();
    }

    public void remove(String id) {
        this.customers.removeIf(c -> c.id.equals(id));
    }

    public void save(AbstractCustomer customer) {
        try {
            remove(customer.id);
            this.customers.add(customer);
            FileWriter fw = new FileWriter(String.valueOf(FILE_PATH));
            for (AbstractCustomer c : customers){
                if (c instanceof RegularCustomer){
                    fw.write("REGULAR" + ";" + c.id + ";" + c.name + ";" + customer.bonusPoints + ";"
                            + ((RegularCustomer) c).lastOrderDate + "\n");
                } else {
                    fw.write("GOLD" + ";" + c.id + ";" + c.name + ";" + customer.bonusPoints + "\n");
                }
            }
            fw.flush();
            fw.close();
        } catch (IOException e){
            throw new RuntimeException("there is no such file");
        }
    }

    public int getCustomerCount() {
        return this.customers.size();
    }
}
