package poly.range;

import java.util.Iterator;

public class SpecialIterable implements Iterable<Integer> {

    private final int start;
    private final int end;

    public SpecialIterable(int start, int end) {
        this.start = start;
        this.end = end;
    }

    @Override
    public Iterator<Integer> iterator() {
        return new SpecialIterator(start, end);
    }
}
