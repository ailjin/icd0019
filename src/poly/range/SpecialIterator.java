package poly.range;

import java.util.Iterator;

public class SpecialIterator implements Iterator<Integer> {

    private int start;
    private int end;

    public SpecialIterator(int start, int end) {
        this.start = start;
        this.end = end;
    }

    @Override
    public boolean hasNext() {
        return start <= end;
    }

    @Override
    public Integer next() {
        return start++;
    }
}
