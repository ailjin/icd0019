package poly.shapes;

public abstract class Shape {
    public abstract Double getArea();
}
