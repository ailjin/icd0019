package poly;

import org.hamcrest.Matcher;
import org.hamcrest.Matchers;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;


public class AverageTest {

    @Test
    public void computesAverageFromListOfNumbers() {

        Double result = getAverage(Arrays.asList(3.9, 4, 1.2, 0, 9, 2));

        assertThat(result, is(closeTo(3.35)));
    }

    public Double getAverage(List<Number> numbers) {
        for (int i = 0; i < numbers.size(); i++){
            if (!(numbers.get(i) instanceof Double)){
                numbers.set(i, numbers.get(i).doubleValue());
            }
        }
        return numbers.stream().mapToDouble(x ->(double) x).average().orElse(0.0);
    }

    private Matcher<Double> closeTo(double value) {
        double precision = 0.001;

        return Matchers.closeTo(value, precision);
    }

}
