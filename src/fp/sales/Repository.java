package fp.sales;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

public class Repository {

    public ArrayList<Entry> entries;
    private static final String FILE_PATH = "src/fp/sales/sales-data.csv";

    private DateTimeFormatter formatter = DateTimeFormatter
            .ofPattern("dd.MM.yyyy");

    public Repository() {
        try {
            ArrayList<Entry> entries = new ArrayList<>();

            int i = 0;

            for (String line : Files.readAllLines(Paths.get(FILE_PATH))) {
                if (i == 0){
                    i++;
                    continue;
                }
                Entry a = new Entry();
                String[] data = line.split("\t");
                a.setProductId(data[2]);
                a.setDate(LocalDate.parse(data[0], formatter));
                a.setState(data[1]);
                a.setCategory(data[3]);
                a.setAmount(Double.valueOf(data[5].replace(",", ".")));
                entries.add(a);
                i++;
            }
            this.entries = entries;
        } catch (IOException e){
            throw new RuntimeException();
        }
    }
}
