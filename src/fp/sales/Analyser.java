package fp.sales;

import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

public class Analyser {


    private Repository repository;

    public Analyser(Repository repository) {
        this.repository = repository;
    }

    public Double getTotalSales() {
        return repository.entries.stream()
                .map(Entry::getAmount)
                .mapToDouble(each -> each)
                .sum();
    }

    public Double getSalesByCategory(String category) {
        return repository.entries.stream()
                .filter(x -> x.getCategory().equals(category))
                .map(Entry::getAmount)
                .mapToDouble(each -> each)
                .sum();
    }

    public Double getSalesBetween(LocalDate start, LocalDate end) {
        return repository.entries.stream()
                .filter(x -> x.getDate().isAfter(start) && x.getDate().isBefore(end))
                .map(Entry::getAmount)
                .mapToDouble(each -> each)
                .sum();
    }

    public String mostExpensiveItems() {
        List<Entry> sorted = repository.entries.stream()
                .sorted(Collections.reverseOrder(Comparator.comparing(Entry::getAmount)))
                .collect(Collectors.toList());
        return sorted.get(2).getProductId() + ", " + sorted.get(0).getProductId() + ", " + sorted.get(1).getProductId();
    }

    public String statesWithBiggestSales() {
        Map<String, Double> map = repository.entries.stream()
                .collect(Collectors.toMap(each -> each.getState(), each -> each.getAmount(), (a, b) -> a + b));

        ArrayList<String> result = new ArrayList<>();

        map.entrySet().stream()
                .sorted(Collections.reverseOrder(Map.Entry.comparingByValue()))
                .forEach(each -> result.add(each.getKey()));
        return result.get(0) + ", " + result.get(1) + ", " + result.get(2);
    }
}
