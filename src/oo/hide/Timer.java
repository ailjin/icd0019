package oo.hide;

public class Timer {

    public double start;
    public double timeBefore = 6.2126E13;

    public Timer(){
        this.start = timeBefore - System.currentTimeMillis();
    }

    public String getPassedTime() {
        return Double.toString (start - (timeBefore - System.currentTimeMillis()));
    }
}
