package oo.hide;

public class Counter {

    public int start;
    public int step;

    public Counter(int start, int step) {
        this.start = start;
        this.step = step;
    }

    boolean atStart = true;

    public int nextValue() {
        int a = start;
        start += step;
        return a;
    }
}
