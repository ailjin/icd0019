package oo.hide;

public class Fibonacci {

    public Integer count = 0;

    public int nextValue() {
        if (count == 0){
            count++;
            return 0;
        }
        if (count == 1){
            count++;
            return 1;
        }
        else {
            Fibonacci recur1 = new Fibonacci();
            Fibonacci recur2 = new Fibonacci();
            recur1.count = this.count - 1;
            recur2.count = this.count - 2;
            count++;
            return recur1.nextValue() + recur2.nextValue();
        }
    }
}
