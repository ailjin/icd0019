package oo.hide;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

public class PointSet {

    Point[] asd;

    public PointSet(int capacity) {
        this.asd = new Point[capacity];
    }

    public PointSet() {
        this(10);
        this.asd = new Point[10];
    }

    public void add(Point point) {
        if (asd[asd.length - 1] != null){
            asd = Arrays.copyOf(asd, asd.length * 2);
        }
        if (!(this.contains(point))){
            for (int i = 0; i <= asd.length; i++){
                if (asd[i] == null) {
                    asd[i] = point;
                    break;
                }
            }
        }
    }

    public int size() {
        int count = 0;
        for (Point elem : asd){
            if (elem != null){
                count += 1;
            }
        }
        return count;
    }

    public boolean contains(Point point) {
        boolean check = false;
        for (Point elem : asd){
            if (elem == null){
                continue;
            }
            if (elem.equals(point)) {
                check = true;
                break;
            }
        }
        return check;
    }

    public String toString(){
        StringBuilder a = new StringBuilder();
        int i = 0;
        for (Point elem : asd){
            if (elem == null){
                continue;
            }
            if (i == this.size() - 1){
                a.append(String.format("(%s, %s)", elem.x, elem.y));
            }
            else {
                a.append(String.format("(%s, %s), ", elem.x, elem.y));
            }
            i += 1;
        }
        return a.toString();
    }

    public boolean equals(Object obj){
        if (!(obj instanceof PointSet)){
            return false;
        }

        PointSet other = (PointSet) obj;

        if (other.size() != this.size()){
            return false;
        }

        for (Point elem : asd){
            if (elem == null){
                continue;
            }
            if (!(other.contains(elem))){
                return false;
            }
        }
        return true;
    }

    public PointSet subtract(PointSet other) {
        PointSet qwe = new PointSet();
        for (Point object : asd) {
            if (!other.contains(object)){
                qwe.add(object);
            }
        }
        return qwe;
    }

    public PointSet intersect(PointSet other) {
        PointSet qwe = new PointSet();
        for (Point object : asd){
            if (other.contains(object)){
                qwe.add(object);
            }
        }
        return qwe;
    }
}
