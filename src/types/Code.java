package types;

public class Code {

    public static void main(String[] args) {

        int[] numbers = {3, 2, 9, 1};

        System.out.println(sum(numbers)); // 11
        System.out.println(average(numbers));
        System.out.println(minimumElement(numbers));
        System.out.println(asString(numbers));
    }

    public static int sum(int[] numbers) {
        int s = 0;
        for (int number : numbers) {
            s += number;
        }
        return s;
    }

    public static double average(int[] numbers) {
        double a = 0.0;
        for (int number : numbers) {
            a += number;
        }
        a = a / numbers.length;
        return a;
    }

    public static Integer minimumElement(int[] integers) {
        if (integers.length < 1) {
            return null;
        }
        int minValue = integers[0];
        for(int integ : integers) {
            if (integ < minValue) {
                minValue = integ;
            }
        }
        return minValue;
    }

    public static String asString(int[] elements) {
        String d = ", ";
        StringBuilder s = new StringBuilder();
        for ( int element : elements ) {
            if (s.length() > 0) {
                s.append(d);
            }
            s.append(element);
        }
        return String.valueOf(s);
    }

    public static String squareDigits(String s) {
        StringBuilder newstr = new StringBuilder();
        char[] ca = s.toCharArray();
        for (char c : ca) {
            if (Character.isDigit(c)) {
                String cc = Character.toString(c);
                int ccc = Integer.parseInt(cc);
                int cccc = ccc * ccc;
                newstr.append(cccc);
            }
            else {
                newstr.append(c);
            }
        }
        return String.valueOf(newstr);
    }


}
