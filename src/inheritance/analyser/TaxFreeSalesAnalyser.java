package inheritance.analyser;

import java.util.List;

public class TaxFreeSalesAnalyser extends Analyser {

    public TaxFreeSalesAnalyser(List<SalesRecord> records) {
        super(records);
        this.records = records;
    }

    @Override
    public Double getTotalSales() {
        Double count = 0.0;
        for (SalesRecord good : records){
            for (int i = 0; i < good.getItemsSold(); i++){
                count += good.getProductPrice();
            }
        }
        return count;
    }
}
