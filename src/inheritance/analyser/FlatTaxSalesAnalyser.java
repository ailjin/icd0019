package inheritance.analyser;

import java.util.List;

public class FlatTaxSalesAnalyser extends Analyser {

    public FlatTaxSalesAnalyser(List<SalesRecord> records) {
        super(records);
        this.records = records;
    }

    @Override
    public Double getTotalSales() {
        double count = 0.0;
        for (SalesRecord good : records){
            for (int i = 0; i < good.getItemsSold(); i++){
                count += good.getProductPrice() / 1.2;
            }
        }
        return count;
    }

    @Override
    public Double getTotalSalesByProductId(String id) {
        return super.getTotalSalesByProductId(id) / 1.2;
    }
}
