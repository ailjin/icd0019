package inheritance.analyser;

import java.util.List;

public abstract class Analyser {

    public List<SalesRecord> records;

    public Analyser(List<SalesRecord> records) {
        this.records = records;
    }

    public abstract Double getTotalSales();

    public Double getTotalSalesByProductId(String id) {
        Double count = 0.0;
        for (SalesRecord good : records){
            if (good.getProductId().equals(id)){
                for (int i = 0; i < good.getItemsSold(); i++){
                    count += good.getProductPrice();
                }
            }
        }
        return count;
    }

    public String getIdOfMostPopularItem() {
        String biggestId = records.get(0).getProductId();
        Integer biggestSold = records.get(0).getItemsSold();
        for (SalesRecord good : records) {
            if (good.getItemsSold() > biggestSold) {
                biggestSold = good.getItemsSold();
                biggestId = good.getProductId();
            }
        }
        return biggestId;
    }

    public String getIdOfItemWithLargestTotalSales() {
        String biggestId = records.get(0).getProductId();
        Double biggestSold = getTotalSalesByProductId(records.get(0).getProductId());
        for (SalesRecord good : records) {
            if (getTotalSalesByProductId(good.getProductId()) > biggestSold) {
                biggestSold = getTotalSalesByProductId(good.getProductId());
                biggestId = good.getProductId();
            }
        }
        return biggestId;
    }


}
