package inheritance.analyser;

import java.util.List;

public class DifferentiatedTaxSalesAnalyser extends Analyser {

    public DifferentiatedTaxSalesAnalyser(List<SalesRecord> records){
        super(records);
        this.records = records;
    }

    @Override
    public Double getTotalSales() {
        double count = 0.0;
        for (SalesRecord good : records){
            for (int i = 0; i < good.getItemsSold(); i++){
                if (good.hasReducedRate()){
                    count += good.getProductPrice() / 1.1;
                } else{
                    count += good.getProductPrice() / 1.2;
                }
            }
        }
        return count;
    }

    @Override
    public Double getTotalSalesByProductId(String id) {
        double count = 0.0;
        for (SalesRecord good : records){
            if (good.getProductId().equals(id)){
                for (int i = 0; i < good.getItemsSold(); i++){
                    if (good.hasReducedRate()){
                        count += good.getProductPrice() / 1.1;
                    } else{
                        count += good.getProductPrice() / 1.2;
                    }
                }
            }
        }
        return count;
    }
}
