package exceptions.basic;

public class TryCatchSample {
    public String readDataFrom(Resource resource) {
        try {
            resource.open();
            String data = resource.read();
            resource.close();
            return data;
        } catch (RuntimeException e) {
            throw new RuntimeException("someDefaultValue");
        }
    }
}
