package exceptions.numbers;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.Properties;

public class NumberConverter {

    String lang = null;
    String filePath = null;
    Properties properties = new Properties();
    FileInputStream is = null;

    public NumberConverter(String lang) {
        this.lang = lang;
        this.filePath = "src/exceptions/numbers/numbers_" + this.lang + ".properties";

        try {
            is = new FileInputStream(filePath);

            InputStreamReader reader = new InputStreamReader(
                    is, StandardCharsets.UTF_8);

            properties.load(reader);
        } catch (IOException e) {
            throw new MissingLanguageFileException(lang, e);
        } catch (IllegalArgumentException e){
            throw new BrokenLanguageFileException(lang, e);
        } finally {
            close(is);
        }
    }

    private static void close(FileInputStream is) {
        if (is == null) {
            return;
        }

        try {
            is.close();
        } catch (IOException ignore) {
        }
    }

    public String numberInWords(Integer number) {
        Properties properties = new Properties();
        FileInputStream is = null;

        try {
            is = new FileInputStream(filePath);

            InputStreamReader reader = new InputStreamReader(
                    is, StandardCharsets.UTF_8);

            properties.load(reader);
        } catch (IOException e) {
            throw new MissingLanguageFileException(lang, e);
        } catch (IllegalArgumentException e){
            throw new BrokenLanguageFileException(lang, e);
        } finally {
            close(is);
        }
        try {
            return helper(properties, number);
        } catch (StringIndexOutOfBoundsException e) {
            throw new MissingTranslationException(lang);
        }
    }
    private String helper(Properties properties,int number){
        int ones = number % 10;
        if (properties.containsKey(String.valueOf(number))) {
            return properties.getProperty(String.valueOf(number));
        } else if (number > 10 && number < 20) {
            return properties.getProperty(String.valueOf(ones)) + properties.getProperty("teen");
        } else if (number > 19 && number < 100) {
            int tens = number / 10;
            if (ones != 0) {
                if (properties.containsKey(tens + "0")) {
                    return properties.getProperty(tens + "0")
                            + properties.getProperty("tens-after-delimiter")
                            + properties.getProperty(String.valueOf(ones));
                } else {
                    return properties.getProperty(String.valueOf(tens))
                            + properties.getProperty("tens-suffix")
                            + properties.getProperty("tens-after-delimiter")
                            + properties.getProperty(String.valueOf(ones));
                }
            } else {
                if (properties.containsKey(tens + "0")) {
                    return properties.getProperty(tens + "0");
                } else {
                    return properties.getProperty(String.valueOf(tens)) + properties.getProperty("tens-suffix");
                }
            }
        } else {
            if (number == 100) {
                return properties.getProperty("1")
                        + properties.getProperty("hundreds-before-delimiter")
                        + properties.getProperty("hundred");
            }
            int hundreds = number / 100;
            int tens = Integer.parseInt(String.valueOf(number).substring(1, 2));
            if (tens == 0 && ones != 0) {
                return properties.getProperty(String.valueOf(hundreds))
                        + properties.getProperty("hundreds-before-delimiter")
                        + properties.getProperty("hundred")
                        + properties.getProperty("hundreds-after-delimiter")
                        + properties.getProperty(String.valueOf(ones));
            } else {
                return properties.getProperty(String.valueOf(hundreds))
                        + properties.getProperty("hundreds-before-delimiter")
                        + properties.getProperty("hundred")
                        + properties.getProperty("hundreds-after-delimiter")
                        + numberInWords(Integer.parseInt(String.valueOf(number).substring(1, 3)));
            }
        }
    }
}