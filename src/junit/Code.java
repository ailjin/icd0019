package junit;

public class Code {

    public static boolean isSpecial(int number) {
        return number % 11 == 0 || (number - 1) % 11 == 0 || (number - 2) % 11 == 0 || (number - 3) % 11 == 0;
    }

    public static int longestStreak(String input) {
        char[] characters = input.toCharArray();
        int countStreak = 0;
        int biggest = 0;
        char countingObject = 'x';
        for (char c : characters) {
            if (countingObject != c) {
                countStreak = 0;
            }
            countingObject = c;
            countStreak += 1;
            if(countStreak > biggest){
                biggest = countStreak;
            }
        }
        return biggest;
    }

    public static Character mode(String input) {
        if(input == null){
            return null;
        }
        char[] array = input.toCharArray();
        int biggest = 0;
        Character modeInInput = null;
        for(char c : array){
            if(getCharacterCount(input, c) > biggest){
                modeInInput = c;
                biggest = getCharacterCount(input, c);
            }
        }
        return modeInInput;
    }

    public static int getCharacterCount(String input, char c) {
        char[] array = input.toCharArray();
        int count = 0;
        for(char cc : array){
            if(c == cc){
                count += 1;
            }
        }
        return count;
    }

    public static int[] removeDuplicates(int[] input) {
        if(input.length < 2){
            return input;
        }
        int[] newArray = new int[input.length];
        int c = 0;
        for(int i : input){
            if(ifContainsElementInArray(newArray, i)){
                continue;
            }
            newArray[c] = i;
            c += 1;
        }
        int i = 0;
        while(i < newArray.length){
            if(newArray[i] == 0){
                newArray = removeElementByIndex(newArray, i);
                continue;
            }
            i ++;
        }
        return newArray;
    }

    public static int[] removeElementByIndex(int[] input, int index){
        int[] newArray = new int[input.length - 1];
        int e = 0;
        for(int j=0; j<=newArray.length; j++){
            if(j == index){
                continue;
            }
            newArray[e] = input[j];
            e += 1;
        }
        return newArray;
    }

    public static boolean ifContainsElementInArray(int[] input, int element){
        for(int i : input){
            if(i == element){
                return true;
            }
        }
        return false;
    }

    public static int sumIgnoringDuplicates(int[] integers) {
        int[] newArray = removeDuplicates(integers);
        int sum = 0;
        for(int i : newArray){
            sum += i;
        }
        return sum;
    }

}

