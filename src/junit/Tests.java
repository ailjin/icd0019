package junit;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.*;

public class Tests {

    @Test
    public void equalityExamples() {
        assertEquals(1, 1);
        assertNotEquals(1, 2);
        int x2 = 1;
        int y2 = 1;
        assertEquals(x2, y2);

        int x = 128;
        int y = 128;
        assertEquals(x, y);
        assertEquals(x, y);

        assertSame("abc", "abc");
        assertSame("abc", "a" + "bc");

        String a = "a";
        assertNotSame("abc", a + "bc");
        assertNotEquals("abc", a + "b");
    }

    @Test
    public void assertThatAndAssertEqualsExample() {
        assertEquals(1 + 2, 3);
        assertThat(1 + 2, is(not(4)));
        assertThat(new int[] {1, 2, 3}, is(equalTo(new int[] {1, 2, 3})));
        assertThat(new int[] {1, 2, 3}, is(not(new int[] {1, 2})));
    }

    @Test
    public void findsSpecialNumbers() {
        assertTrue(Code.isSpecial(0));

        // other test cases for isSpecial() method
        assertTrue(Code.isSpecial(1));
        assertTrue(Code.isSpecial(2));
        assertTrue(Code.isSpecial(3));
        assertFalse(Code.isSpecial(4));
        assertTrue(Code.isSpecial(11));
        assertFalse(Code.isSpecial(15));
        assertTrue(Code.isSpecial(36));
        assertFalse(Code.isSpecial(37));
    }

    @Test
    public void findsLongestStreak() {
        assertThat(Code.longestStreak(""), is(0));
        assertThat(Code.longestStreak("a"), is(1));
        assertThat(Code.longestStreak("abbcccaaaad"), is(4));
        // other test cases for longestStreak() method
    }

    @Test
    public void findsModeFromCharactersInString() {

        assertThat(Code.mode(null), is(nullValue()));

        // other test cases for mode() method
        assertThat(Code.mode(""), is(nullValue()));
        assertThat(Code.mode("abcb"), is('b'));
        assertThat(Code.mode("cbbc"), is('c'));
    }

    @Test
    public void getsCharecterCountFromString(){
        assertThat(Code.getCharacterCount("", 'a'), is(0));
        assertThat(Code.getCharacterCount("abaca", 'a'), is(3));
        assertThat(Code.getCharacterCount("sdfgv", 'a'), is(0));
        assertThat(Code.getCharacterCount("aaaaa", 'a'), is(5));
    }

    @Test
    public void removesDuplicates() {
        assertThat(Code.removeDuplicates(arrayOf(1, 1)), is(arrayOf(1)));

        assertThat(Code.removeDuplicates(arrayOf(1, 2, 1, 2)), is(arrayOf(1, 2)));

        assertThat(Code.removeDuplicates(arrayOf(1, 2, 3)), is(arrayOf(1, 2, 3)));
    }

    @Test
    public void sumsIgnoringDuplicates() {
        assertThat(Code.sumIgnoringDuplicates(arrayOf(1, 1)), is(1));

        assertThat(Code.sumIgnoringDuplicates(arrayOf(1, 2, 1, 2)), is(3));

        assertThat(Code.sumIgnoringDuplicates(arrayOf(1, 2, 3)), is(6));
    }

    private int[] arrayOf(int... numbers) {
        return numbers;
    }

}
